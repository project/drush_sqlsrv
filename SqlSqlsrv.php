<?php

namespace Drush\Sql;

use Consolidation\SiteProcess\Util\Escape;
use Drush\Drush;
use Drush\Exec\ExecTrait;

class SqlSqlsrv extends SqlBase
{
    use ExecTrait;

    /**
     * The unix command used to connect to the database.
     * @return string
     */
    public function command()
    {
        return 'sqlcmd';
    }

    /**
     * Build a fragment connection parameters.
     *
     * @param bool $hide_password
     *  If TRUE, DBMS should try to hide password from process list.
     *  On mysql, that means using --defaults-file to supply the user+password.
     * @return string
     */
    public function creds($hide_password = true)
    {
        // Some drush commands (e.g. site-install) want to connect to the
        // server, but not the database.  Connect to the built-in database.
        $dbSpec = $this->getDbSpec();
        $database = empty($dbSpec['database']) ? 'master' : $dbSpec['database'];
        // Host and port are optional but have defaults.
        $host = empty($dbSpec['host']) ? '.\SQLEXPRESS' : $dbSpec['host'];
        if ($dbSpec['username'] == '') {
            return ' -S ' . $host . ' -d ' . $database;
        }
        return ' -S ' . $host . ' -d ' . $database . ' -U ' . $dbSpec['username'] . ' -P ' . $dbSpec['password'];
    }

    /**
     * Build a SQL string for dropping and creating a database.
     *
     * @param string dbname
     *   The database name.
     * @param boolean $quoted
     *   Quote the database name. Mysql uses backticks to quote which can cause problems
     *   in a Windows shell. Set TRUE if the CREATE is not running on the bash command line.
     * @return string
     */
    public function createdbSql($dbname, $quoted = false)
    {
        return 'CREATE DATABASE ' . $dbname;
    }

    /**
     * @inheritdoc
     */
    public function dumpCmd($table_selection)
    {
        $parens = false;
        $skip_tables = $table_selection['skip'];
        $structure_tables = $table_selection['structure'];
        $tables = $table_selection['tables'];

        $ignores = [];
        $skip_tables  = array_merge($structure_tables, $skip_tables);
        $data_only = $this->getOption('data-only');

        $create_db = $this->getOption('create-db');

        $exec = 'sqlcmd  ';
        $extra = $this->creds();
        if ($data_only) {
            //$extra .= ' --no-create-info';
        }
        if ($option = $this->getOption('extra-dump')) {
            $extra .= " $option";
        }
        $exec .= $extra;
        $dbSpec = $this->getDbSpec();
        $exec .= " -Q BACKUP DATABASE [" . $dbSpec['database'] . "]";

        return $parens ? "($exec)" : $exec;
    }

    /**
     * @inheritDoc
     */
    public function listTables()
    {
        $return = $this->alwaysQuery('SELECT TABLE_NAME FROM information_schema.tables');
        $tables = drush_shell_exec_output();
        if (!empty($tables)) {
            // Shift off the header of the column of data returned.
            array_shift($tables);
            return $tables;
        }
    }

    /**
     * @inheritDoc
     */
    public function drop($tables)
    {
        if ($tables) {
            foreach ($tables as $table) {
                $sql = "DROP TABLE \"$table\"";
                $return = $this->alwaysQuery($sql);
                if (!$return) {
                    throw new \Exception('Failed to drop table ' . $table);
                }
            }
        }
        return TRUE;
    }

    /**
     * @inheritDoc
     */
    public function dbExists()
    {
        // Suppress output. We only care about return value.
        return $this->alwaysQuery("SELECT 1;");
    }


}
